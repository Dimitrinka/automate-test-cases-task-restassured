Trello REST API: https://developer.atlassian.com/cloud/trello/rest/api-group-actions/

Test tool: RESTAssured and TestNG. A skeleton is available in the Alpha 38 QA group repo (https://gitlab.com/TelerikAcademy/alpha-38-qa/-/tree/main/Module%203/01.%20Web%20Services%20EXAM/RESTAssured-EXAM-Skeleton)

Task 1

Create a test to validate your initial setup via Trello REST API and RESTAssured

Use the Trello API key and token
Follow the best testing practices
Validate authentication

Task 2

Create a board in Trello via Trello REST API and RESTAssured

Use the Trello API key and token
Follow the best practices and naming conventions
Validate board creation

Task 3

Create a list in Trello via Trello REST API and RESTAssured

Use the Trello API key and token
Follow the best practices and naming conventions
Validate list creation

Task 4

Create a card in Trello via Trello REST API and RESTAssured

Use the Trello API key and token
Reuse the newly created list in Task 3
Follow the best practices and naming conventions
Validate card creation

Task 5

Update the card in Trello via Trello REST API and RESTAssured

Use the Trello API key and token
Set/update card name, description
Follow the best practices and naming conventions
Validate card name and description

