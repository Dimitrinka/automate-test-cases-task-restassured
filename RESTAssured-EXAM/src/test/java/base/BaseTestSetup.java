package base;

import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.response.Response;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import static tests.Constants.*;
import static tests.Endpoints.BOARD_ENDPOINT;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.testng.Assert.assertTrue;

public class BaseTestSetup {
    @BeforeMethod
    public void setup() {
        EncoderConfig encoderConfig = RestAssured.config().getEncoderConfig()
                .appendDefaultContentCharsetToContentTypeIfUndefined(false);

        RestAssured.config = RestAssured.config().encoderConfig(encoderConfig);
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuite() {

        baseURI = format("%s%s/%s", BASE_URL, BOARD_ENDPOINT, BOARD_ID);

        Response response = given()
                .queryParam("key", KEY)
                .queryParam("token", TOKEN)
                .when()
                .delete();

        int statusCode = response.getStatusCode();
        assertTrue(statusCode == 200 || statusCode == 404, "Incorrect status code. Expected 200 or 404.");

        System.out.printf("Board with id %s was deleted.%n", BOARD_ID);
    }
}
