package trello;

import base.BaseTestSetup;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

import static tests.Constants.*;
import static tests.Endpoints.BOARD_ENDPOINT;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.testng.Assert.assertEquals;

public class BoardTest extends BaseTestSetup {
    @Test
    public void createBoardTest() {

        baseURI = format("%s%s", BASE_URL, BOARD_ENDPOINT);

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss");
        LocalDateTime localDateTime = LocalDateTime.now();
        String boardNumber = localDateTime.format(dateTimeFormatter);
        String boardUniqueName = format("%s %s", BOARD_NAME, boardNumber);

        Response response = given()
                .queryParam("name", boardUniqueName)
                .queryParam("key", KEY)
                .queryParam("token", TOKEN)
                .when()
                .post();

        int statusCode = response.getStatusCode();
        
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");
        assertEquals(response.getBody().jsonPath().getString("name"), boardUniqueName,
                "Board names do not match.");

        BOARD_ID = response.getBody().jsonPath().getString("id");

        System.out.printf("Board with name %s and id %s was created.%n", boardUniqueName, BOARD_ID);
    }

    @Test
    public void getBoardListsTest() {

        baseURI = format("%s%s/%s/lists", BASE_URL, BOARD_ENDPOINT, BOARD_ID);

        Response response = given()
                .queryParam("key", KEY)
                .queryParam("token", TOKEN)
                .when()
                .get();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");

        ArrayList<HashMap<String, String>> lists = response.getBody().jsonPath().get();
        for (HashMap<String, String> list : lists)
            if (list.get("name").equals("To Do")) {
                TO_DO_LIST_ID = list.get("id");
                break;
            }

        System.out.printf("Board List 'To Do' id is: %s.%n", TO_DO_LIST_ID);
    }
}
