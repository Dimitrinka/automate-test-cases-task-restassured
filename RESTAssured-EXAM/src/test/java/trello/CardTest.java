package trello;
import base.BaseTestSetup;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import java.util.Map;

import static tests.Constants.*;
import static tests.Endpoints.CARD_ENDPOINT;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.testng.Assert.assertEquals;

public class CardTest extends BaseTestSetup{
    @Test
    public void createCardTest() {

        baseURI = format("%s%s", BASE_URL, CARD_ENDPOINT);

        Response response = given()
                .queryParam("idList", TO_DO_LIST_ID)
                .queryParam("key", KEY)
                .queryParam("token", TOKEN)
                .when()
                .post();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");

        CARD_ID = response.getBody().jsonPath().getString("id");

        System.out.printf("Card with name %s and id %s was created.%n", CARD_NAME, CARD_ID);
    }

    @Test
    public void updateCardQueryParamTest() {

        baseURI = format("%s%s/%s", BASE_URL, CARD_ENDPOINT, CARD_ID);

        Response response = given()
                .queryParam("name", CARD_NAME)
                .queryParam("desc", CARD_DESCRIPTION)
                .queryParam("key", KEY)
                .queryParam("token", TOKEN)
                .when()
                .put();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");
        assertEquals(response.getBody().jsonPath().getString("name"), CARD_NAME,
                "Card names don't match.");
        assertEquals(response.getBody().jsonPath().getString("desc"), CARD_DESCRIPTION,
                "Card description don't match.");

        System.out.println("Card name and description was updated.");
    }

    @Test
    public void moveCardToAnotherListTest() {

        baseURI = format("%s%s/%s", BASE_URL, CARD_ENDPOINT, CARD_ID);

        Response response = given()
                .queryParam("idList", IN_LIST_ID)
                .queryParam("key", KEY)
                .queryParam("token", TOKEN)
                .when()
                .put();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");
        assertEquals(response.getBody().jsonPath().getString("idList"), IN_LIST_ID,
                "Card list names don't match.");

        System.out.println("Card is move to 'In Test' list");
    }

}
