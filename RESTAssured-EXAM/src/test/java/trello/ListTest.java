package trello;

import base.BaseTestSetup;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static tests.Constants.*;
import static tests.Endpoints.BOARD_ENDPOINT;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.testng.Assert.assertEquals;

public class ListTest extends BaseTestSetup {

    @Test
    public void createListTest() {

        baseURI = format("%s%s/%s/lists", BASE_URL, BOARD_ENDPOINT, BOARD_ID);

        Response response = given()
                .queryParam("name", NEW_LIST_NAME)
                .queryParam("key", KEY)
                .queryParam("token", TOKEN)
                .when()
                .post();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");
        assertEquals(response.getBody().jsonPath().getString("name"), NEW_LIST_NAME,
                "List names don't match.");
        assertEquals(response.getBody().jsonPath().getString("idBoard"), BOARD_ID,
                "Board ids don't match.");

        IN_LIST_ID = response.getBody().jsonPath().getString("id");
    }
}

