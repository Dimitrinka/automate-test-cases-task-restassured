package tests;

public class Endpoints {
    public static final String AUTH_ENDPOINT = "/1/members/me";
    public static final String BOARD_ENDPOINT = "/1/boards";
    public static final String CARD_ENDPOINT = "/1/card";

}
