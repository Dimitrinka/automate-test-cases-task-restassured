package tests;

public class Constants {

    public static final String KEY = "d9a2317c3d8a40f1cf29d160cd5e6cc9";
    public static final String TOKEN = "addceadea58dde6ec89a6c49c11a6bb5e572b7464852185e766467184cd5e210";
    public static final String BASE_URL = "https://api.trello.com";
    public static final String FULL_NAME = "Dimitrinka Dimitrova";
    public static final String BOARD_NAME = "Web Services";
    public static final String NEW_LIST_NAME = "In Test";
    public static String BOARD_ID;
    public static String IN_LIST_ID;
    public static String TO_DO_LIST_ID;
    public static String CARD_ID;
    public static final String CARD_NAME = "Get your exam";
    public static final String CARD_DESCRIPTION = "Trello REST API: https://developer.atlassian.com/cloud/trello/rest/api-group-actions/";
}
